# BlDriveTest 

## Prerequisites

### Hardware

- BlHeli32 or KISS ESC supporting DSHOT and Telemetry
- STM32 Board with BlDriveTest Firmware

![Connection Diagram](connection.png)

### Python Installation 

- Python 3.x
- pyserial, pyqt5, pyqtgraph

The Modules can be installed using pip

`py -m pip install --user pyserial pyqt5 pyqtgraph`

## Variants

| Name/Pinout     | Command           | Measurement               |
| --------------- | ----------------- | ------------------------- |
| TELEMETRY-DSHOT | DShot             | Telemetry                 |
| RPM-PWM         | PWM (900-2100 us) | RPM                       |
| ANALOG-PWM      | PWM (900-2100 us) | Analog Current            |
| RPM-PWM-ANALOG  | PWM (900-2100 us) | RPM + extra Analog (U, I) |

## Usage

`drivetest.py`

![screenshot](screenshot.png)

## Development Notes

### Protocol

#### Logdata

`\n` `'n'` `'m'` `length` `payload` `crc16-xmodem`

```c
typedef struct drivestate {
	uint32_t cnt;
	uint16_t cmd;
	uint8_t reserved;
	uint8_t T_degC;
	uint16_t U_10mV;
	uint16_t I_10mA;
	uint16_t C_1mAh;
	uint16_t f_100eRPM;
} drivestate_t;
```

#### Commands

A single byte as defined in `hw_config.c` and `main.c`.

| Value   | PWM Command                       | DSHOT Command                       |
| ------- | --------------------------------- | ----------------------------------- |
| 0       | 900                               | 0                                   |
| 1-47    | 900                               | 1-47, e.g. for beeps, reverse, etc. |
| 48-127  | unchanged, used for other purpose | unchanged, used for other purpose   |
| 128-137 | 900-990 us with 10 us/LSB         | 0                                   |
| 138-238 | 1000-2000 us with 10 us/LSB       | 48-2047 (237→ 2028, 238 → 2047)     |
| 239-248 | 2010-2100 us                      | 2047                                |
| 249-255 |                                   |                                     |



| Value     | Purpose                                      |
| --------- | -------------------------------------------- |
| 'h' (104) | Heartbeat signal that resets timeout counter |
| 'r' (114) | 1/s rate limit                               |
| 'j' (106) | no rate limit                                |

