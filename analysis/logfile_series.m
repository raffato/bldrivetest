% get file names
[FileName,PathName] = uigetfile('*','Select binary log file(s)','MultiSelect', 'on');

File = {};
if iscell(FileName) || ischar(FileName)
    if iscell(FileName)
        File = FileName;
    else
        File = {FileName};
    end
    fstatic = figure('units','normalized','outerposition',[0.2 0.2 0.6 0.6]);
else
    disp('No files selected.');
end

for i=1:length(File)
    %% parse data
    control_data = logfile_parse([PathName,File{i}]);

    %% plot static points
    figure(fstatic);
    logfile_static(control_data,File{i});

end