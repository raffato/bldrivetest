function logfile_plot(varargin)
    %% default parameters
    if (nargin == 1)
        control_data = varargin{1};
    else
        control_data = evalin('base', 'control_data');
    end
    if (LogName(end-15:end) == 'UI_reversed.uart')
        disp('UI reversed.');
        tmp = control_data.bltest.U_10mV.Data;
        control_data.bltest.U_10mV.Data = control_data.bltest.I_10mA.Data;
        control_data.bltest.I_10mA.Data = tmp;
    end
    
    %% sensor type
    sensor = 'dshot';
    if strcmp(sensor, 'dshot')
        U_off = 0;
        U_slope = 1e-2;
        I_off = 0;
        I_slope = 1e-2;
    elseif strcmp(sensor, 'mauch')
        U_off = -0.0733*26.59;
        U_slope = 3.4517/4096.0*26.59;
        I_off = 0;
        I_slope = 3.4517/4096.0*60.5;
    elseif strcmp(sensor, 'drotek')
        U_off = -0.0733*15.55;
        U_slope = 3.4517/4096.0*15.55;
        I_off = -1.65/4096.0*151.5*0.97;
        I_slope = 3.4517/4096.0*151.5*0.97;
    end
    
    %% control outputs
    axis_e = [];

    if isfield(control_data, 'bltest')
        if isfield(control_data.bltest, 'U_10mV')
            axis_e(end+1) = subplot(2,2,3);
            U = U_off+single(control_data.bltest.U_10mV.Data)*U_slope;
            plot(control_data.bltest.U_10mV.Time, movmean(U,10));
            grid on;
            xlabel('t [s]');
            ylabel('U [V]');
            linkaxes(axis_e,'x');
            title('Voltage','Interpreter','none');
        end
        if isfield(control_data.bltest, 'I_10mA')
            axis_e(end+1) = subplot(2,2,4);
            I = I_off+single(control_data.bltest.I_10mA.Data)*I_slope;
            plot(control_data.bltest.I_10mA.Time, movmean(I,50));
            hold on
            str = sprintf('Current (%.0f mAh used)',sum(I)/1000/3.6);
            grid on;
            xlabel('t [s]');
            ylabel('I [A]');
            linkaxes(axis_e,'x');
            title(str,'Interpreter','none');
        end
        if isfield(control_data.bltest, 'f_100eRPM')
            axis_e(end+1) = subplot(2,2,2);
            plot(control_data.bltest.f_100eRPM.Time, movmean(single(control_data.bltest.f_100eRPM.Data)*1e2/7,1));
            grid on;
            hold on;
            %plot(control_data.bltest.cmd.Time, (single(control_data.bltest.cmd.Data)-1000)*7.2);
            xlabel('t [s]');
            ylabel('f [RPM]');
            linkaxes(axis_e,'x');
            title('Speed','Interpreter','none');
        end
        if isfield(control_data.bltest, 'cmd')
            axis_e(end+1) = subplot(2,2,1);
            plot(control_data.bltest.cmd.Time, single(control_data.bltest.cmd.Data));
            grid on;
            xlabel('t [s]');
            ylabel('cmd []');
            linkaxes(axis_e,'x');
            title('Command','Interpreter','none');
        end
    end