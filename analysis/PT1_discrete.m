Tf = 0.005;
Ts = 0.001;

% comparison of linear and discrete transfer functions
G_pt1 = tf([0 1],[Tf 1]);
K_pt1_z = c2d(G_pt1,Ts,'zoh')
K_pt1_f = c2d(G_pt1,Ts,'foh')
K_pt1_i = c2d(G_pt1,Ts,'impulse')
K_pt1_t = c2d(G_pt1,Ts,'tustin')
K_pt1_m = c2d(G_pt1,Ts,'matched')

h = bodeplot(G_pt1,K_pt1_f,K_pt1_t);
setoptions(h,'FreqUnits','Hz');
legend('show')