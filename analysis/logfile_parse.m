function busdata = logfile_parse(filename)
    %logfile_parse  Parse a binary log file with simulink data.
    %   busdata = logfile_parse(filename) parses file and returns a struct
    %   with vector data and busses as timeseries objects
    %
    %   (c) 2016 Thomas Raffler
    
    %% Read and parse file
    fileID = fopen(filename);
    [raw,cnt_bytes] = fread(fileID,'uint8=>uint8');
    fclose(fileID);

    % timing start
    tStart = tic; 
    % Parse raw frames
    [data, frame, cnt_ignored, cnt_crcerror, cnt_lengtherror] = parse_frames(raw, cnt_bytes);
    
    busdata = [];
    % load bus definition
    control_BusDef();
    
    %% manually defined frames

    %% fully bus defined frames
    if isfield(data, 'x6D')
        busdata.bltest = data2bus(data.x6D, frame.x6D, 'BlDriveTest_Bus');
    end

    %% display statistics
    elapsedTime = toc(tStart);
    fprintf('Parsed %d bytes in %f seconds. %d bytes were ignored. \n', cnt_bytes, elapsedTime, cnt_ignored);
end

function busdata = data2bus(framedata, framenumber, BusName)
    % Remove empty, preallocated rows and convert to timeseries
    if evalin('base',['exist(''' BusName ''',''var'')'])
        struct = raw2struct(framedata(1:framenumber,:), evalin('base', BusName));
        t = double(struct.Timestamp_ms)/1000;
        busdata = struct2bus(struct, t);
        fprintf('Parsed %d frames of type %s. \n', length(t), BusName);
    else
        warning('Base Workspace has no Bus called %s. \n', BusName);
        busdata = NaN;
    end
end

function struct = struct2bus(struct, t)
    fields = fieldnames(struct);
    for i = 1:numel(fields)
        struct.(fields{i}) = timeseries(struct.(fields{i}), t, 'Name', fields{i});
    end
end

function BusStruct = raw2struct(data, BusDef)
    BusStruct = [];
    dsize = struct('double',{8},'single',{4},'int8',{1},'int16',{2},'int32',{4},'int64',{8},'uint8',{1},'uint16',{2},'uint32',{4},'uint64',{8});
    index = 1;
    for element=BusDef.Elements'
        if strcmp(element.DataType, 'boolean')
            BusStruct.(element.Name) = logical(reshape(data(:,index)',1,[]));
            index = index + 1;
        else
            dwidth = dsize.(element.DataType);
            % padding
            while (mod(index-1,dwidth))
                index = index + 1;
            end
            dim = element.Dimensions;
            dset = [];
            for i=1:dim
                dset = [dset; typecast(reshape(data(:,index+dwidth*(i-1):index+dwidth*i-1)',1,[]),element.DataType)];
            end
            BusStruct.(element.Name) = dset';
            index = index + dwidth*dim;
        end
    end
end


function [data, frame, cnt_ignored, cnt_crcerror, cnt_lengtherror] = parse_frames(raw, cnt_bytes)
    pos = uint32(1);
    cnt_ignored = 0;
    cnt_crcerror = 0;
    cnt_lengtherror = 0;

    sof1 = uint8(hex2dec('0a'));
    sof2 = uint8('n');

    data = [];
    frame = [];
    ids = [ones(256,1)*'x' dec2hex(0:255)];
    %% Parse raw data
    while (cnt_bytes > pos+4)
        % find header and try to parse data
        if raw(pos) == sof1 && raw(pos+1) == sof2
            id = ids(raw(pos+2)+1,:);
            len = uint32(raw(pos+3));
            % check if there is enough data left
            if cnt_bytes >= pos+len+7 && raw(pos+len+6) == sof1 && raw(pos+len+7) == sof2
                % Check CRC
                crc16_comp = crc16(raw(pos+2:pos+len+3));
                crc16_rx = uint16(raw(pos+4+len)) + uint16(raw(pos+4+len+1))*256;

                % Parse data
                if crc16_comp == crc16_rx
                    if isfield(data, id)
                        if (size(data.(id)(frame.(id)+1,:),2) == len)
                            data.(id)(frame.(id)+1,:) = raw(pos+4:pos+len+4-1);
                            frame.(id) = frame.(id) + 1;
                        end
                    else
                        % initialize data matrix with maximum size
                        data.(id) = zeros(floor(cnt_bytes/(len+6)),len,'uint8');
                        frame.(id) = 0;
                    end
                    pos = pos + uint32(len) + 6;
                else
                    cnt_crcerror = cnt_crcerror + 1;
                    pos = pos + 1;
                    cnt_ignored = cnt_ignored + 1;
                end
            else
                cnt_lengtherror = cnt_lengtherror + 1;
                pos = pos + 1;
                cnt_ignored = cnt_ignored + 1;
                continue
            end
        else
            pos = pos + 1;
            cnt_ignored = cnt_ignored + 1;
        end
    end
end