function logfile_static(varargin)
    %% default argument
    if (nargin == 1)
        control_data = varargin{1};
        logname = '';
    elseif (nargin == 2)
        control_data = varargin{1};
        logname = varargin{2};
    else
        control_data = evalin('base', 'control_data');
    end
    
    %% Parameters
    % Measurement
    sfRPM = 1e2/14;
    sfCurrent = 1e-2;
    offsetCurrent = 0;
    sfVoltage = 1e-2;
    offsetVoltage = 0;
    % Processing
    tSettle = 0.1;
    tAverage = 0.8;
    tSample = 0.001;
    
    %% Identify cmd jumps and static values
    jumps = find(diff(single(control_data.bltest.cmd.Data))~=0);

    cmd = zeros(length(jumps),1);
    rpm = zeros(length(jumps),1);
    I = zeros(length(jumps),1);
    U =  zeros(length(jumps),1);

    for i=1:length(jumps)-1
        iStart = jumps(i) + round(tSettle/tSample);
        if jumps(i+1) < iStart + round(tAverage/tSample)
            % take data until next jump
            iEnd = jumps(i+1);
        else
            % take limited data range
            iEnd = iStart + round(tAverage/tSample);
        end

        cmd(i) = control_data.bltest.cmd.Data(iStart);
        rpm(i) = mean(double(control_data.bltest.f_100eRPM.Data(iStart:iEnd)))*sfRPM;
        I(i) = mean(double(control_data.bltest.I_10mA.Data(iStart:iEnd)))*sfCurrent-offsetCurrent;
        U(i) =  mean(double(control_data.bltest.U_10mV.Data(iStart:iEnd)))*sfVoltage-offsetVoltage;
    end
    
    %% Dynamics
    %     plot(tSample*(0:iEnd-iStart), movmean(single(control_data.bltest.I_10mA.Data(iStart:iEnd))*1e-2,10));
    %     grid on;
    %     hold on;
    %     xlabel('t [s]');
    %     ylabel('I [A]');
    %     title('Current','Interpreter','none');
    
    %% Plot figures
    subplot(1,2,1);
    plot(U.*(cmd-1000)/1000, rpm,'x','DisplayName',logname);
    grid on;
    hold on;
    xlabel('PWM Voltage [V]');
    ylabel('rpm [1/min]');
    title('RPM','Interpreter','none');
    %legend('Interpreter','none');

    subplot(1,2,2);
    plot(U.*(cmd-1000)/1000, U.*I,'x','DisplayName',logname);
    grid on;
    hold on;
    xlabel('PWM Voltage [V]');
    ylabel('Power [W]');
    title('Power','Interpreter','none');
    %legend('Interpreter','none');
end
