% get file name
[FileName,PathName] = uigetfile('*','Select the binary log file');

if FileName ~= 0
    %% parse data
    control_data = logfile_parse([PathName,FileName]);
    
    %% plot full log
    figure('Name',FileName,'units','normalized','outerposition',[0 0 1 1])
    logfile_plot(control_data)
    
    %% plot static points
    figure('Name',FileName,'units','normalized','outerposition',[0.2 0.2 0.6 0.6]);
    logfile_static(control_data)
end