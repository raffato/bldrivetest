/* (c) 2018, Thomas Raffler */

#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include "diag/Trace.h"

#include "main.h"

#include "stm32f10x_conf.h"

#include "system.h"
#include "pwm_output.h"
#include "communication.h"
#include "adc.h"
#include "dshot.h"
#include "uart.h"

#include "hw_config.h"
#include "usb_lib.h"
#include "usb_desc.h"
#include "usb_pwr.h"

void usb_message(void *payload, uint8_t length, uint8_t txid);
uint8_t update_crc8(uint8_t crc, uint8_t crc_seed);
void rpm_capture_init (void);

const cmd_mode_t mode=cmd_dshot;
const capture_mode_t capture=capture_dshot;
const uint32_t T_filter_us=5000;
int32_t usb_cmd=0, ramp=1;
uint32_t cmd_timeout=1000;

static volatile uint16_t cntLast=0, rpm_timeout=0, f_Hz=0;
// frequency capturing for 20-20.000 Hz
void TIM2_IRQHandler(void) {
	uint16_t cntNew, delta;

	TIM_ClearITPendingBit(TIM2, TIM_IT_CC4);
	cntNew = TIM2->CCR4;

	if (rpm_timeout < 60) {
		static uint32_t f_filtered=0;
		// delta T
		if (cntNew > cntLast) {
			delta = cntNew - cntLast;
		}
		else {
			delta = 0xffff - cntLast + cntNew; // timer wrapped over
		}
		/*
		 * first order low pass filter with variable sample time
		 * f = 1/delta_t, with delta_t = delta/1000000;
		 * f_filtered = (1-alpha)*f_filtered + alpha*f, with alpha = Ts/(T_filter+Ts)
		 * f_filtered = (T_filter*f_filtered + 1)/(T_filter + delta_t)
		 */
		f_filtered = (T_filter_us*f_filtered + 1000000)/(T_filter_us+delta); // in us
		f_Hz = (uint16_t) f_filtered;
	}

	rpm_timeout = 0;
	cntLast = cntNew;
}

int main(void)
{
	uartPort_t* TelemetryUART=NULL;
	drivestate_t drive={0, 0, 0, 0, 0, 0, 0, 0};
	uint32_t syncerror=0;

	system_init(1); // 1000 Hz
	Set_System();
	Set_USBClock();
	USB_Interrupts_Config();
	USB_Init();
	if (capture == capture_dshot) {
		TelemetryUART = uart_open(USART2, 115200, MODE_8N1, MODE_RX);
	}
	else if (capture == capture_rpm_analog) {
		adc_init();
		rpm_capture_init();
	}
    else if (capture == capture_analog_cjmcu) {
        adc_init();
    }

	if (mode == cmd_dshot) {
		dshot_init();
	}
	else {
		pwm_init(mode);
	}
	LED_ON;
	delay_ms(500);
	trace_puts("Ready to start drive test.");
	while (1) {

		// slow down loop to desired sample rate
		timer_sync();
		LED_OFF;

		// get desired command
		drive.cmd = usb_cmd;

		// output motor command
		if (cmd_timeout < 200) {
			cmd_timeout++; // resets when usb receives heartbeat
			// generate output
			if (mode == cmd_dshot) {
				// convert to dshot command
				if (drive.cmd >= 1000) {
					// on (
					dshot_cmd(48 + (drive.cmd - 1000)*2, 1);
				}
				else if (drive.cmd < 48){
					// special commands
					dshot_cmd(drive.cmd, 1);
					drive.cmd = 0;
				}
				else if (drive.cmd > 0) {
					// off
					dshot_cmd(0, 1);
				}
			}
			else {
				// pwm pulse scheduler
				static uint16_t pwm_sched=0;
				// reduce rate to 250 Hz for standard pwm
				if (mode != cmd_standard || pwm_sched++ >= 4-1) {
					pwm_sched = 0;
					if (drive.cmd >= 900) {
						pwm_pulse(drive.cmd);
					}
					else {
						pwm_pulse(900);
					}
				}
			}
		}

		// get measurements
		if (capture == capture_dshot) {
			// Read dshot telemetry
			uint8_t NrOfBytes = uart_bytes_waiting(TelemetryUART);
			uint8_t telemetry_new=0;

			while (NrOfBytes > 0) {
				static uint8_t crc=0, NrRead=0, Buffer[10]={0};
				uint8_t byte = uart_read(TelemetryUART);
				NrOfBytes--;
				Buffer[NrRead] = byte;
				NrRead++;
				if (NrRead < 10) {
					crc = update_crc8(byte, crc);
				}
				else if (byte == crc){
					crc = 0;
					NrRead=0;
					drive.T_degC = Buffer[0];
					drive.U_10mV = (uint16_t)Buffer[1]<<8 | Buffer[2];
					drive.I_10mA = (uint16_t)Buffer[3]<<8 | Buffer[4];
					drive.C_1mAh = (uint16_t)Buffer[5]<<8 | Buffer[6];
					drive.f_100eRPM = (uint16_t)Buffer[7]<<8 | Buffer[8];
					telemetry_new=1;
					LED_ON;
				}
				else {
					// shift back to find start
					Buffer[0] = byte;
					crc = update_crc8(byte, 0);
					NrRead=1;
					trace_puts("m");
					syncerror++;
				}
			}
		}
		else if (capture == capture_rpm_analog) {
			drive.T_degC = 25;
			drive.U_10mV = adc1_read(4); // U_10mV = adc*128/100 for drotek sensor
			drive.I_10mA = adc1_read(5); // I_10mA = (adc-2048)*500/4096 for 6.6 mV/A of ACS759
			drive.C_1mAh = 0;
			if (rpm_timeout >= 60) {
				drive.f_100eRPM = 0;
			}
			else {
				drive.f_100eRPM = 60*f_Hz/100;
				rpm_timeout++;
			}
		}
        else if (capture == capture_analog_cjmcu) {
            drive.T_degC = 25;
            drive.U_10mV = 0;
            drive.I_10mA = adc1_read(3);
            drive.C_1mAh = 0;
            drive.f_100eRPM = 0;
        }

		// timestamp and data out
		if (drive.cnt == 0 && drive.cmd != 0) {
			// first pwm command starts counter and messages
			drive.cnt++;
			usb_message(&drive, sizeof(drive), 'm');
		}
		else if (drive.cnt != 0) {
			drive.cnt++;
			usb_message(&drive, sizeof(drive), 'm');
		}
		else {
			usb_message(&drive, sizeof(drive), 'm');
		}
	}
}

void usb_message(void *payload, uint8_t length, uint8_t txid) {
	uint16_t crc=0;
	uint8_t *pData = (uint8_t*)payload;

	USB_Send_Byte('\n');
	USB_Send_Byte('n');
	USB_Send_Byte(txid);
	USB_Send_Byte(length);
	USB_Send_Data(pData, length);
	crc16xmodem_update(&crc, txid);
	crc16xmodem_update(&crc, length);
	crc = crc16xmodem(crc, pData, length);
	USB_Send_Byte((uint8_t)(crc & 0xff));
	USB_Send_Byte((uint8_t)(crc >> 8));
}

uint8_t update_crc8(uint8_t crc, uint8_t crc_seed) {
	uint8_t crc_u, i;
	crc_u = crc;
	crc_u ^= crc_seed;
	for (i=0; i<8; i++)
		crc_u = ( crc_u & 0x80 ) ? 0x7 ^ ( crc_u << 1 ) : ( crc_u << 1 );
	return (crc_u);
}

void rpm_capture_init (void)
{
	TIM_TimeBaseInitTypeDef  TimeBase;
	GPIO_InitTypeDef GPIO_Struct;
	TIM_ICInitTypeDef  TIM_ICInitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;

	// Timer clock activation
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);
	// Base timer config
	TimeBase.TIM_ClockDivision = 0;
	TimeBase.TIM_CounterMode = TIM_CounterMode_Up;
	TimeBase.TIM_RepetitionCounter = 0x0000;
	TimeBase.TIM_Prescaler = 72 - 1; // 72 MHz / (TIM_Prescaler + 1) = 1 MHz
	TimeBase.TIM_Period = 0xffff; // overflows every 65 ms
	TIM_TimeBaseInit(TIM2, &TimeBase);
	TIM_Cmd(TIM2, ENABLE);

	/* Pin config PA3*/
	GPIO_Struct.GPIO_Pin = GPIO_Pin_3;
	GPIO_Struct.GPIO_Mode = GPIO_Mode_IPD;
	GPIO_Struct.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_Init(GPIOA, &GPIO_Struct);

	/* TIM2 global Interrupt*/
	NVIC_InitStructure.NVIC_IRQChannel = TIM2_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

	/* Input Capture mode TIM2 CH4 pin (PA3) */
	TIM_ICInitStructure.TIM_Channel = TIM_Channel_4;
	TIM_ICInitStructure.TIM_ICPolarity = TIM_ICPolarity_Falling;
	TIM_ICInitStructure.TIM_ICSelection = TIM_ICSelection_DirectTI;
	TIM_ICInitStructure.TIM_ICPrescaler = TIM_ICPSC_DIV1;
	TIM_ICInitStructure.TIM_ICFilter = 0x04;
	TIM_ICInit(TIM2, &TIM_ICInitStructure);

	/* Enable the CC4 Interrupt Request */
	TIM_ITConfig(TIM2, TIM_IT_CC4, ENABLE);
}

#pragma GCC diagnostic pop
