/* (c) 2015, Thomas Raffler */

#pragma once


#define F_CONTROL 800u
#define F_SERVOS 50u
#define ARMTIME F_CONTROL/2u
#define PWM_MIN 980
#define PI_F 3.14159265358979323846f


typedef struct drivestate {
	uint32_t cnt;
	uint16_t cmd;
	uint8_t reserved;
	uint8_t T_degC;
	uint16_t U_10mV;
	int16_t I_10mA;
	uint16_t C_1mAh;
	uint16_t f_100eRPM;
} drivestate_t;


typedef enum capture_mode {
    capture_rpm_analog = 0,
    capture_analog_cjmcu = 1,
    capture_dshot = 2
} capture_mode_t;
