/* (c) 2015, Thomas Raffler */

#pragma once


typedef enum pwm_mode {
	cmd_standard = 0,
	cmd_oneshot = 1,
	cmd_dshot = 2
} cmd_mode_t;

#define PWM_MAX 2050
#define TIM1_REMAP

void pwm_init(cmd_mode_t mode_motor);
void pwm_pulse(uint16_t pwmOut);
