/* (c) 2016, Thomas Raffler */

#pragma once

#include <stdint.h>

void adc_init(void);
uint16_t adc1_read(uint8_t channel);
