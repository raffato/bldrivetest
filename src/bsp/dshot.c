/* (c) 2015, Thomas Raffler */
#include <stdint.h>

#include <stdlib.h>
#include <string.h>

#include "stm32f10x_conf.h"
#include "dshot.h"

#define DSHOT_PAUSE 2
#define DSHOT_BITS 16
#define DSHOT_DMA_BUFFER_SIZE (DSHOT_BITS + DSHOT_PAUSE)

#define DSHOT_PWM_0 15
#define DSHOT_PWM_1 30

/* DSHOT
 * TIM1/1N, PA7
 *
 * fTimer: 72/3 = 24 MHz -> 41.66 ns timestep
 * Period: 40 tic -> 1.666 us, 600 kHz
 * 0 code: pwm 15 -> 0.625 us
 * 1 code: pwm 30 -> 1.250 us
 */

uint8_t dshot_DMABuffer[DSHOT_DMA_BUFFER_SIZE]={0};
volatile uint8_t transfer_flag=0;

void dshot_init(void) {

	TIM_OCInitTypeDef  TIM_OCInitStructure;
	TIM_TimeBaseInitTypeDef  TimeBase;
	DMA_InitTypeDef DMA_Struct;
	GPIO_InitTypeDef GPIO_Struct;

	// GPIO
	GPIO_Struct.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_Struct.GPIO_Speed = GPIO_Speed_50MHz;

	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
	GPIO_Struct.GPIO_Pin = GPIO_Pin_7;
	GPIO_Init(GPIOA, &GPIO_Struct);
	GPIO_PinRemapConfig(GPIO_PartialRemap_TIM1, ENABLE);

	// timer config
	TimeBase.TIM_ClockDivision = 0;
	TimeBase.TIM_CounterMode = TIM_CounterMode_Up;
	TimeBase.TIM_RepetitionCounter = 0x0000;
	TimeBase.TIM_Prescaler = 3 - 1; // 72 MHz / (TIM_Prescaler + 1) = 24 MHz
	TimeBase.TIM_Period = 40 - 1; // 600 kHz pwm frequency

	RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM1, ENABLE);
	TIM_TimeBaseInit(TIM1, &TimeBase);

	// output compare
	TIM_OCStructInit(&TIM_OCInitStructure);
	TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1;
	TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
	TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High;
	TIM_OCInitStructure.TIM_Pulse = 0;

	TIM_OCInitStructure.TIM_OutputNState = TIM_OutputNState_Enable;
	TIM_OCInitStructure.TIM_OCNPolarity = TIM_OCNPolarity_Low;
	TIM_OCInitStructure.TIM_OCIdleState = TIM_OCIdleState_Reset;
	TIM_OCInitStructure.TIM_OCNIdleState = TIM_OCNIdleState_Reset;
	TIM_OC1NPolarityConfig(TIM1, TIM_OCPolarity_High);

	TIM_OC1Init(TIM1, &TIM_OCInitStructure);
	TIM_OC1PreloadConfig(TIM1, TIM_OCPreload_Enable);
	TIM_CtrlPWMOutputs(TIM1, ENABLE);
	TIM_Cmd(TIM1, ENABLE);


	// DMA
	// clock enable */
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1, ENABLE);

	// DMA1 Channel2
	DMA_DeInit(DMA1_Channel2);

	DMA_StructInit(&DMA_Struct);
	DMA_Struct.DMA_PeripheralBaseAddr = (uint32_t)&TIM1->CCR1;
	DMA_Struct.DMA_MemoryBaseAddr = (uint32_t)dshot_DMABuffer;
	DMA_Struct.DMA_DIR = DMA_DIR_PeripheralDST;
	DMA_Struct.DMA_BufferSize = DSHOT_DMA_BUFFER_SIZE;
	DMA_Struct.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
	DMA_Struct.DMA_MemoryInc = DMA_MemoryInc_Enable;
	DMA_Struct.DMA_PeripheralDataSize = DMA_PeripheralDataSize_HalfWord;
	DMA_Struct.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
	DMA_Struct.DMA_Mode = DMA_Mode_Normal;
	DMA_Struct.DMA_Priority = DMA_Priority_High;
	DMA_Struct.DMA_M2M = DMA_M2M_Disable;

	DMA_Init(DMA1_Channel2, &DMA_Struct);

	/* TIM1 CC1 DMA Request enable */
	TIM_DMACmd(TIM1, TIM_DMA_CC1, ENABLE);
	DMA_ITConfig(DMA1_Channel2, DMA_IT_TC, ENABLE);


	NVIC_InitTypeDef NVIC_InitStructure;
	NVIC_InitStructure.NVIC_IRQChannel = DMA1_Channel2_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 2;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

	memset(dshot_DMABuffer, DSHOT_PWM_0, sizeof(dshot_DMABuffer)-DSHOT_PAUSE);
}

void dshot_cmd(uint16_t cmd, uint8_t telemetry) {
	uint8_t bit_cnt=0;

	if (cmd > 2047)
		cmd = 2047;

	uint16_t packet = (cmd << 1) | (telemetry ? 1 : 0);
	uint16_t crc = 0, crc_data = packet;
	for (uint8_t i = 0; i < 3; i++) {
	    crc ^=  crc_data;   // xor data by nibbles
	    crc_data >>= 4;
	}
	crc &= 0xf;

	// rgb, most significant bit first
	dshot_DMABuffer[bit_cnt++] = (cmd & (1 <<10)) ? DSHOT_PWM_1 : DSHOT_PWM_0;
	dshot_DMABuffer[bit_cnt++] = (cmd & (1 << 9)) ? DSHOT_PWM_1 : DSHOT_PWM_0;
	dshot_DMABuffer[bit_cnt++] = (cmd & (1 << 8)) ? DSHOT_PWM_1 : DSHOT_PWM_0;
	dshot_DMABuffer[bit_cnt++] = (cmd & (1 << 7)) ? DSHOT_PWM_1 : DSHOT_PWM_0;
	dshot_DMABuffer[bit_cnt++] = (cmd & (1 << 6)) ? DSHOT_PWM_1 : DSHOT_PWM_0;
	dshot_DMABuffer[bit_cnt++] = (cmd & (1 << 5)) ? DSHOT_PWM_1 : DSHOT_PWM_0;
	dshot_DMABuffer[bit_cnt++] = (cmd & (1 << 4)) ? DSHOT_PWM_1 : DSHOT_PWM_0;
	dshot_DMABuffer[bit_cnt++] = (cmd & (1 << 3)) ? DSHOT_PWM_1 : DSHOT_PWM_0;
	dshot_DMABuffer[bit_cnt++] = (cmd & (1 << 2)) ? DSHOT_PWM_1 : DSHOT_PWM_0;
	dshot_DMABuffer[bit_cnt++] = (cmd & (1 << 1)) ? DSHOT_PWM_1 : DSHOT_PWM_0;
	dshot_DMABuffer[bit_cnt++] = (cmd & (1 << 0)) ? DSHOT_PWM_1 : DSHOT_PWM_0;
	// telemetry request
	dshot_DMABuffer[bit_cnt++] = (  telemetry   ) ? DSHOT_PWM_1 : DSHOT_PWM_0;
	// crc
	dshot_DMABuffer[bit_cnt++] = (crc & (1 << 3)) ? DSHOT_PWM_1 : DSHOT_PWM_0;
	dshot_DMABuffer[bit_cnt++] = (crc & (1 << 2)) ? DSHOT_PWM_1 : DSHOT_PWM_0;
	dshot_DMABuffer[bit_cnt++] = (crc & (1 << 1)) ? DSHOT_PWM_1 : DSHOT_PWM_0;
	dshot_DMABuffer[bit_cnt++] = (crc & (1 << 0)) ? DSHOT_PWM_1 : DSHOT_PWM_0;

	if (transfer_flag==0) {
		transfer_flag=1;
		TIM_SetCounter(TIM1, 0);
		DMA_SetCurrDataCounter(DMA1_Channel2, DSHOT_DMA_BUFFER_SIZE);
		DMA_Cmd(DMA1_Channel2, ENABLE);
	}
}

void DMA1_Channel2_IRQHandler(void)
{
	// stop dma pwm
	if (DMA_GetFlagStatus(DMA1_FLAG_TC2)) {
		DMA_Cmd(DMA1_Channel2, DISABLE);
		DMA_ClearFlag(DMA1_FLAG_TC2);
		transfer_flag=0;
	}
}
