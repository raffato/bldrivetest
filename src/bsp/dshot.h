/* (c) 2018, Thomas Raffler */

#pragma once

#include <stdint.h>

void dshot_init(void);
void dshot_cmd(uint16_t cmd, uint8_t telemetry);
