/* (c) 2015, Thomas Raffler */

#pragma once

#include <stdint.h>

#define WS2811_LENGTH 10
//#define WS2811_PWM_CH6
#define WS2811_TIM1_PA7

#define WS2811_GRB
//#define WS2811_RGB
//#define WS2811_W

#if defined(WS2811_W)
	#define WS2811_COMPS 4
#else
	#define WS2811_COMPS 3
#endif
void ws2811_init(void);

void ws2811_pixel(uint8_t *rgb, uint16_t pixel_no);
void ws2811_set_pixel(uint8_t *rgb, uint16_t pixel_no);
void ws2811_transfer(void);
void ws2811_clear(void);
