/* (c) 2015, Thomas Raffler */

#pragma once

#include <stdint.h>
#include <stddef.h>

/* Calculate CRC16 (CRC-CCITT, XModem) using table-driven method
* crc - initial value of CRC
* data - data pointer
* len - length of the data
*/
uint16_t crc16xmodem(uint16_t crc, const uint8_t *data, size_t len);

/* Calculate CRC16 (CRC-CCITT, XModem) using table-driven method
* crc - pointer to initial value of CRC
* byte - data byte to process
*/
void crc16xmodem_update(uint16_t *crc, const uint8_t byte);

/* Calculate CRC16 (CRC-CCITT, Kermit)
* crc - initial value of CRC
* data - data pointer
* len - length of the data
*/
uint16_t crc16kermit(uint16_t crc, uint8_t *data, uint16_t len);

/* Calculate CRC16 (CRC-CCITT, Kermit)
* crc - pointer to initial value of CRC
* byte - data byte to process
*/
void crc16kermit_update(uint16_t *crc, uint8_t data);
