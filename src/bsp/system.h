/* (c) 2015, Thomas Raffler */

#pragma once

// leds
#define LED_ON                 GPIO_ResetBits(GPIOB, GPIO_Pin_12)
#define LED_OFF                GPIO_SetBits(GPIOB, GPIO_Pin_12)

void system_init(uint32_t T_base_ms);
void reset_to_bootloader(void);
void delay_us(uint32_t us);
void delay_ms(uint32_t ms);
void timer_sync(void);
uint8_t button1_read(void);
uint8_t button2_read(void);
uint32_t micros(void);
uint32_t millis(void);
