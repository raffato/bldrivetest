/* (c) 2015, Thomas Raffler */

#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

#include "stm32f10x_gpio.h"
#include "system.h"
#include "cmsis_device.h"

// cycles per microsecond
static volatile uint32_t usTicks = 0;
// current uptime for 1kHz systick timer. will rollover after 49 days. hopefully we won't care.
static volatile uint32_t sysTickUptime = 0;
// debounce counter
static uint16_t debounce_cnt=10;

void button_debounce(uint8_t button_input, uint8_t* button_state, uint8_t* button_cnt);

// SysTick
void SysTick_Handler(void)
{
	sysTickUptime++;
}

// Return system uptime in microseconds (rollover in 70minutes)
uint32_t micros(void)
{
	register uint32_t ms, cycle_cnt;
	do {
		ms = sysTickUptime;
		cycle_cnt = SysTick->VAL;
	} while (ms != sysTickUptime);
	return (ms * 1000) + (usTicks * 1000 - cycle_cnt) / usTicks;
}

// Return system uptime in milliseconds (rollover in 49 days)
uint32_t millis(void)
{
	return sysTickUptime;
}

void system_init(uint32_t T_base_ms)
{
	GPIO_InitTypeDef gpio;
	TIM_TimeBaseInitTypeDef  TimeBase;
	RCC_ClocksTypeDef clocks;

#ifdef OPBL
	NVIC_SetVectorTable(NVIC_VectTab_FLASH, 0x3000);
#endif
	// Configure NVIC preempt/priority groups
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);

	// Turn on clocks for stuff we use
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO, ENABLE);

	RCC_ClearFlag();

	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA | RCC_APB2Periph_GPIOB | RCC_APB2Periph_GPIOC, ENABLE);


	gpio.GPIO_Mode = GPIO_Mode_AIN;
	gpio.GPIO_Pin = GPIO_Pin_All;
	GPIO_Init(GPIOA, &gpio);
	GPIO_Init(GPIOB, &gpio);
	GPIO_Init(GPIOC, &gpio);

	// LED init
	gpio.GPIO_Pin = GPIO_Pin_12;
	gpio.GPIO_Mode = GPIO_Mode_Out_PP;
	gpio.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_Init(GPIOB, &gpio);

	LED_OFF;

	// Timer clock activation
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM4, ENABLE);
	// Base timer config
	TimeBase.TIM_ClockDivision = 0;
	TimeBase.TIM_CounterMode = TIM_CounterMode_Up;
	TimeBase.TIM_RepetitionCounter = 0x0000;
	TimeBase.TIM_Prescaler = 72 - 1; // 72 MHz / (TIM_Prescaler + 1) = 1 MHz
	TimeBase.TIM_Period = T_base_ms*1000 - 1; // ms timebase
	TIM_TimeBaseInit(TIM4, &TimeBase);
	TIM_Cmd(TIM4, ENABLE);

	debounce_cnt = 100/T_base_ms;

	// Init cycle counter
	RCC_GetClocksFreq(&clocks);
	usTicks = clocks.SYSCLK_Frequency / 1000000;
	SysTick_Config(SystemCoreClock / 1000);

	delay_ms(100);
}

void delay_us(uint32_t us)
{
	uint32_t now = micros();
	while (micros() - now < us);
}

void delay_ms(uint32_t ms)
{
	while (ms--)
		delay_us(1000);
}

void timer_sync(void)
{
	// Sync with PWM timer
	while ((TIM4->SR & TIM_SR_UIF) == 0);
		TIM4->SR &= ~TIM_SR_UIF;
}

void button_debounce(uint8_t button_input, uint8_t* button_state, uint8_t* button_cnt) {
	if (button_input != *button_state && *button_cnt == 0) {
		*button_state = button_input;
		*button_cnt = debounce_cnt;
	}
	else if (*button_cnt > 0){
		(*button_cnt)--;
	}
}

uint8_t button1_read(void) {
	static uint8_t button_y=1, button_cnt=0;

	uint8_t button_u = GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_2);
	button_debounce(button_u, &button_y, &button_cnt);
	return button_y;
}

uint8_t button2_read(void) {
	static uint8_t button_y=1, button_cnt=0;

	uint8_t button_u = GPIO_ReadInputDataBit(GPIOB, GPIO_Pin_1);
	button_debounce(button_u, &button_y, &button_cnt);
	return button_y;
}

void reset_to_bootloader(void) {
	uint32_t *pMagicValue = (uint32_t *)0x20003FF0; // somewhere in RAM...

	// Magic number to jump into bootloader
	*pMagicValue = 0xFEE1DEAD;
	NVIC_SystemReset();
}
