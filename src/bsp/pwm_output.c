/* (c) 2015, Thomas Raffler */

#include <stdbool.h>
#include <stdint.h>

#include <stdlib.h>

#include "stm32f10x_conf.h"

#include "pwm_output.h"

void pwm_init(cmd_mode_t mode_motor) {

	/* cjmcu-jlink:
	 * SWDIO: PA7 - TIM3/2 or TIM1/1N (remapped)
	 */

	TIM_OCInitTypeDef  TIM_OCInitStructure;
	TIM_TimeBaseInitTypeDef  TimeBaseMotors;
	GPIO_InitTypeDef GPIO_Struct;

	// GPIO
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
	GPIO_Struct.GPIO_Pin = GPIO_Pin_7;
	GPIO_Struct.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_Struct.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_Init(GPIOA, &GPIO_Struct);

	// Motor timer config
	TimeBaseMotors.TIM_ClockDivision = 0;
	TimeBaseMotors.TIM_CounterMode = TIM_CounterMode_Up;
	TimeBaseMotors.TIM_RepetitionCounter = 0x0000;
	if (mode_motor == cmd_oneshot) {
		TimeBaseMotors.TIM_Prescaler = 9 - 1; // 72 MHz / (TIM_Prescaler + 1) = 8 MHz
		TimeBaseMotors.TIM_Period = PWM_MAX; // pwm frequency
	}
	else {
		TimeBaseMotors.TIM_Prescaler = 72 - 1; // 72 MHz / (TIM_Prescaler + 1) = 1 MHz
		TimeBaseMotors.TIM_Period = PWM_MAX; // pwm frequency
	}

#ifdef TIM1_REMAP
	// Remap
	GPIO_PinRemapConfig(GPIO_PartialRemap_TIM1, ENABLE);

	// Peripheral clock activation
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM1, ENABLE);

	TIM_TimeBaseInit(TIM1, &TimeBaseMotors);

	// One-pulse compare configuration
	TIM_OCStructInit(&TIM_OCInitStructure);
	TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM2;
	TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
	TIM_OCInitStructure.TIM_OutputNState = TIM_OutputNState_Enable;
	TIM_OCInitStructure.TIM_Pulse = 1 + PWM_MAX;
	TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High;
	TIM_OCInitStructure.TIM_OCNPolarity = TIM_OCNPolarity_Low;
	TIM_OCInitStructure.TIM_OCIdleState = TIM_OCIdleState_Reset;
	TIM_OCInitStructure.TIM_OCNIdleState = TIM_OCNIdleState_Reset;

	TIM_OC1NPolarityConfig(TIM1, TIM_OCPolarity_High);
	TIM_CtrlPWMOutputs(TIM1, ENABLE);
	TIM_OC1Init(TIM1, &TIM_OCInitStructure);
	TIM_OC1PreloadConfig(TIM1, TIM_OCPreload_Enable);
	TIM_SelectOnePulseMode(TIM1, TIM_OPMode_Single);
#else
	// Peripheral clock activation
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE);

	TIM_TimeBaseInit(TIM3, &TimeBaseMotors);

	// One-pulse compare configuration
	TIM_OCStructInit(&TIM_OCInitStructure);
	TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM2;
	TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
	TIM_OCInitStructure.TIM_OutputNState = TIM_OutputNState_Disable;
	TIM_OCInitStructure.TIM_Pulse = 1 + PWM_MAX;
	TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High;
	TIM_OCInitStructure.TIM_OCIdleState = TIM_OCIdleState_Reset;

	TIM_OC2Init(TIM3, &TIM_OCInitStructure);
	TIM_OC2PreloadConfig(TIM3, TIM_OCPreload_Enable);
	TIM_SelectOnePulseMode(TIM3, TIM_OPMode_Single);
#endif
}

void pwm_pulse(uint16_t pwmOut) {
	// limit range
	if (pwmOut > PWM_MAX) {
		pwmOut = PWM_MAX;
	}

#ifdef TIM1_REMAP
	// set compare register
	TIM1->CCR1 = 1 + PWM_MAX - pwmOut;
	// start timer to generate one pulse
	TIM1->CR1 |= TIM_CR1_CEN;
#else
	// set compare register
	TIM3->CCR2 = 1 + PWM_MAX - pwmOut;
	// start timer to generate one pulse
	TIM3->CR1 |= TIM_CR1_CEN;
#endif
}
