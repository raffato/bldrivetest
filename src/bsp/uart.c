/*
 * Simple UART port abstraction loosely based on cleanflight serial library
 */

#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

#include "system.h"
#include "stm32f10x_gpio.h"

#include "uart.h"

#define UART1_RX_BUFFER_SIZE    0
#define UART1_TX_BUFFER_SIZE    0
#define UART2_RX_BUFFER_SIZE    128
#define UART2_TX_BUFFER_SIZE    0
#define UART3_RX_BUFFER_SIZE    0
#define UART3_TX_BUFFER_SIZE    0

uartPort_t *serialUSART1(uartMode_t mode);
uartPort_t *serialUSART2(uartMode_t mode);
uartPort_t *serialUSART3(uartMode_t mode);

static uartPort_t uartPort1;
static uartPort_t uartPort2;
static uartPort_t uartPort3;

void uartStartTxDMA(uartPort_t *s);
static void uartConfigure(uartPort_t *uartPort, uint32_t baudRate, uartFormat_t format, uartMode_t mode);

uartPort_t *serialUSART1(uartMode_t mode)
{
	uartPort_t *s;
	static volatile uint8_t rx1Buffer[UART1_RX_BUFFER_SIZE];
	static volatile uint8_t tx1Buffer[UART1_TX_BUFFER_SIZE];
	GPIO_InitTypeDef GPIO_Struct;
	NVIC_InitTypeDef NVIC_InitStructure;

	s = &uartPort1;

	s->rxBuffer = rx1Buffer;
	s->txBuffer = tx1Buffer;
	s->rxBufferSize = UART1_RX_BUFFER_SIZE;
	s->txBufferSize = UART1_TX_BUFFER_SIZE;

	s->USARTx = USART1;

	s->txDMAPeripheralBaseAddr = (uint32_t)&s->USARTx->DR;
	s->rxDMAPeripheralBaseAddr = (uint32_t)&s->USARTx->DR;

	s->rxDMAChannel = DMA1_Channel5;
	s->txDMAChannel = DMA1_Channel4;

	RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE);
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1, ENABLE);

	// USART1_TX	PA9
	// USART1_RX	PA10
	GPIO_Struct.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_Struct.GPIO_Pin = GPIO_Pin_9;
	GPIO_Struct.GPIO_Mode = GPIO_Mode_AF_PP;
	if (mode & MODE_TX)
		GPIO_Init(GPIOA, &GPIO_Struct);
	GPIO_Struct.GPIO_Pin = GPIO_Pin_10;
	GPIO_Struct.GPIO_Mode = GPIO_Mode_IPU;
	if (mode & MODE_RX)
		GPIO_Init(GPIOA, &GPIO_Struct);

	// DMA TX Interrupt
	NVIC_InitStructure.NVIC_IRQChannel = DMA1_Channel4_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

	return s;
}

#if (UART1_TX_BUFFER_SIZE > 0)
// USART1 Tx DMA Handler
void DMA1_Channel4_IRQHandler(void)
{
	uartPort_t *s = &uartPort1;
	DMA_ClearITPendingBit(DMA1_IT_TC4);
	DMA_Cmd(s->txDMAChannel, DISABLE);

	if (s->txBufferHead != s->txBufferTail)
		uartStartTxDMA(s);
	else
		s->txDMAEmpty = true;
}
#endif

uartPort_t *serialUSART2(uartMode_t mode)
{
	uartPort_t *s;
	static volatile uint8_t rx2Buffer[UART2_RX_BUFFER_SIZE];
	static volatile uint8_t tx2Buffer[UART2_TX_BUFFER_SIZE];
	GPIO_InitTypeDef GPIO_Struct;
	NVIC_InitTypeDef NVIC_InitStructure;

	s = &uartPort2;

	s->rxBuffer = rx2Buffer;
	s->txBuffer = tx2Buffer;
	s->rxBufferSize = UART2_RX_BUFFER_SIZE;
	s->txBufferSize = UART2_TX_BUFFER_SIZE;

	s->USARTx = USART2;

	s->txDMAPeripheralBaseAddr = (uint32_t)&s->USARTx->DR;
	s->rxDMAPeripheralBaseAddr = (uint32_t)&s->USARTx->DR;

	s->rxDMAChannel = DMA1_Channel6;
	s->txDMAChannel = DMA1_Channel7;

	RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2, ENABLE);
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1, ENABLE);

	// USART2_TX	PA2
	// USART2_RX	PA3
	GPIO_Struct.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_Struct.GPIO_Pin = GPIO_Pin_2;
	GPIO_Struct.GPIO_Mode = GPIO_Mode_AF_PP;
	if (mode & MODE_TX)
		GPIO_Init(GPIOA, &GPIO_Struct);
	GPIO_Struct.GPIO_Pin = GPIO_Pin_3;
	GPIO_Struct.GPIO_Mode = GPIO_Mode_IPU;
	if (mode & MODE_RX)
		GPIO_Init(GPIOA, &GPIO_Struct);

	// DMA TX Interrupt
	NVIC_InitStructure.NVIC_IRQChannel = DMA1_Channel7_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

	return s;
}

#if (UART2_TX_BUFFER_SIZE > 0)
// USART2 Tx DMA Handler
void DMA1_Channel7_IRQHandler(void)
{
	uartPort_t *s = &uartPort2;
	DMA_ClearITPendingBit(DMA1_IT_TC7);
	DMA_Cmd(s->txDMAChannel, DISABLE);

	if (s->txBufferHead != s->txBufferTail)
		uartStartTxDMA(s);
	else
		s->txDMAEmpty = true;
}
#endif

uartPort_t *serialUSART3(uartMode_t mode)
{
	uartPort_t *s;
	static volatile uint8_t rx3Buffer[UART3_RX_BUFFER_SIZE];
	static volatile uint8_t tx3Buffer[UART3_TX_BUFFER_SIZE];
	GPIO_InitTypeDef GPIO_Struct;
	NVIC_InitTypeDef NVIC_InitStructure;

	s = &uartPort3;

	s->rxBuffer = rx3Buffer;
	s->txBuffer = tx3Buffer;
	s->rxBufferSize = UART3_RX_BUFFER_SIZE;
	s->txBufferSize = UART3_TX_BUFFER_SIZE;

	s->USARTx = USART3;

	s->txDMAPeripheralBaseAddr = (uint32_t)&s->USARTx->DR;
	s->rxDMAPeripheralBaseAddr = (uint32_t)&s->USARTx->DR;

	s->rxDMAChannel = DMA1_Channel3;
	s->txDMAChannel = DMA1_Channel2;

	RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART3, ENABLE);
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1, ENABLE);

	// USART3_TX	PB10
	// USART3_RX	PB11
	GPIO_Struct.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_Struct.GPIO_Pin = GPIO_Pin_10;
	GPIO_Struct.GPIO_Mode = GPIO_Mode_AF_PP;
	if (mode & MODE_TX)
		GPIO_Init(GPIOB, &GPIO_Struct);
	GPIO_Struct.GPIO_Pin = GPIO_Pin_11;
	GPIO_Struct.GPIO_Mode = GPIO_Mode_IPU;
	if (mode & MODE_RX)
		GPIO_Init(GPIOB, &GPIO_Struct);

	// DMA TX Interrupt
	NVIC_InitStructure.NVIC_IRQChannel = DMA1_Channel2_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

	return s;
}
// USART3 Tx DMA Handler
#if (UART3_TX_BUFFER_SIZE > 0)
void DMA1_Channel2_IRQHandler(void)
{
	uartPort_t *s = &uartPort3;
	DMA_ClearITPendingBit(DMA1_IT_TC2);
	DMA_Cmd(s->txDMAChannel, DISABLE);

	if (s->txBufferHead != s->txBufferTail)
		uartStartTxDMA(s);
	else
		s->txDMAEmpty = true;
}
#endif
static void uartConfigure(uartPort_t *uartPort, uint32_t baudRate, uartFormat_t format, uartMode_t mode)
{
	USART_InitTypeDef USART_InitStructure;

	USART_InitStructure.USART_BaudRate = baudRate;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = mode;

	if (format == MODE_8E2) {
		USART_InitStructure.USART_StopBits = USART_StopBits_2;
		USART_InitStructure.USART_Parity = USART_Parity_Even;
	}
	else if (format == MODE_8E1) {
		USART_InitStructure.USART_StopBits = USART_StopBits_1;
		USART_InitStructure.USART_Parity = USART_Parity_Even;
	}
	else if (format == MODE_8O2) {
		USART_InitStructure.USART_StopBits = USART_StopBits_2;
		USART_InitStructure.USART_Parity = USART_Parity_Odd;
	}
	else if (format == MODE_8O1) {
		USART_InitStructure.USART_StopBits = USART_StopBits_1;
		USART_InitStructure.USART_Parity = USART_Parity_Odd;
	}
	else if (format == MODE_8N2) {
		USART_InitStructure.USART_StopBits = USART_StopBits_2;
		USART_InitStructure.USART_Parity = USART_Parity_No;
	}
	else {
		USART_InitStructure.USART_StopBits = USART_StopBits_1;
		USART_InitStructure.USART_Parity = USART_Parity_No;
	}

	USART_Init(uartPort->USARTx, &USART_InitStructure);
}

uartPort_t *uart_open(USART_TypeDef *USARTx, uint32_t baudRate, uartFormat_t format, uartMode_t mode)
{
	DMA_InitTypeDef DMA_InitStructure;

	uartPort_t *s = NULL;

	if (USARTx == USART1) {
		s = serialUSART1(mode);
	}
#ifdef UART2_TX_BUFFER_SIZE
	else if (USARTx == USART2) {
		s = serialUSART2(mode);
	}
#endif
#ifdef UART3_TX_BUFFER_SIZE
	else if (USARTx == USART3) {
		s = serialUSART3(mode);
	}
#endif
	else {
		return (uartPort_t *)s;
	}

	uartConfigure(s, baudRate, format, mode);

	DMA_StructInit(&DMA_InitStructure);
	DMA_InitStructure.DMA_PeripheralBaseAddr = s->rxDMAPeripheralBaseAddr;
	DMA_InitStructure.DMA_Priority = DMA_Priority_Medium;
	DMA_InitStructure.DMA_M2M = DMA_M2M_Disable;
	DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
	DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;
	DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
	DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;

	// Receive DMA
	if (mode & MODE_RX) {
		DMA_InitStructure.DMA_BufferSize = s->rxBufferSize;
		DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralSRC;
		DMA_InitStructure.DMA_Mode = DMA_Mode_Circular;
		DMA_InitStructure.DMA_MemoryBaseAddr = (uint32_t)s->rxBuffer;
		DMA_DeInit(s->rxDMAChannel);
		DMA_Init(s->rxDMAChannel, &DMA_InitStructure);
		DMA_Cmd(s->rxDMAChannel, ENABLE);
		USART_DMACmd(s->USARTx, USART_DMAReq_Rx, ENABLE);
		s->rxDMAPos = DMA_GetCurrDataCounter(s->rxDMAChannel);
	}

	DMA_StructInit(&DMA_InitStructure);
	DMA_InitStructure.DMA_PeripheralBaseAddr = s->txDMAPeripheralBaseAddr;
	DMA_InitStructure.DMA_Priority = DMA_Priority_Medium;
	DMA_InitStructure.DMA_M2M = DMA_M2M_Disable;
	DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
	DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;
	DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
	DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;

	// Transmit DMA
	if (mode & MODE_TX) {
		DMA_InitStructure.DMA_BufferSize = s->txBufferSize;
		DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralDST;
		DMA_InitStructure.DMA_Mode = DMA_Mode_Normal;
		DMA_DeInit(s->txDMAChannel);
		DMA_Init(s->txDMAChannel, &DMA_InitStructure);
		DMA_ITConfig(s->txDMAChannel, DMA_IT_TC, ENABLE);
		DMA_SetCurrDataCounter(s->txDMAChannel, 0);
		s->txDMAChannel->CNDTR = 0;
		USART_DMACmd(s->USARTx, USART_DMAReq_Tx, ENABLE);
	}

	USART_Cmd(s->USARTx, ENABLE);

	return (uartPort_t *)s;
}

void uartStartTxDMA(uartPort_t *s)
{
	s->txDMAChannel->CMAR = (uint32_t)&s->txBuffer[s->txBufferTail];
	if (s->txBufferHead > s->txBufferTail) {
		s->txDMAChannel->CNDTR = s->txBufferHead - s->txBufferTail;
		s->txBufferTail = s->txBufferHead;
	} else {
		s->txDMAChannel->CNDTR = s->txBufferSize - s->txBufferTail;
		s->txBufferTail = 0;
	}
	DMA_Cmd(s->txDMAChannel, ENABLE);
}

uint8_t uart_bytes_waiting(uartPort_t *s)
{
	return (s->rxDMAPos - s->rxDMAChannel->CNDTR) & (s->rxBufferSize - 1);
}

uint8_t uart_read(uartPort_t *s)
{
	uint8_t ch;

	ch = s->rxBuffer[s->rxBufferSize - s->rxDMAPos];
	if (--s->rxDMAPos == 0)
		s->rxDMAPos = s->rxBufferSize;

	return ch;
}

void uart_flush(uartPort_t *s)
{
	s->rxDMAPos = s->rxDMAChannel->CNDTR;
}

void uart_write(uartPort_t *s, uint8_t ch)
{
	s->txBuffer[s->txBufferHead] = ch;
	s->txBufferHead = (s->txBufferHead + 1) % s->txBufferSize;

	if (!(s->txDMAChannel->CCR & 1))
		uartStartTxDMA(s);

}

void uart_put(uartPort_t *s, uint8_t ch)
{
	s->txBuffer[s->txBufferHead] = ch;
	s->txBufferHead = (s->txBufferHead + 1) % s->txBufferSize;
}

void uart_puts(uartPort_t *s, void *pData, uint32_t len)
{
	uint8_t *data = (uint8_t *)pData;
	uint32_t i;
	for (i=0; i<len; i++) {
		s->txBuffer[s->txBufferHead] = data[i];
		s->txBufferHead = (s->txBufferHead + 1) % s->txBufferSize;
	}
}

void uart_send(uartPort_t *s)
{
	if (!(s->txDMAChannel->CCR & 1))
		uartStartTxDMA(s);

}

void uart_set_baudrate(uartPort_t *s, uint32_t baudrate)
{
	uint32_t tmpreg = 0x00, apbclock = 0x00;
	uint32_t integerdivider = 0x00;
	uint32_t fractionaldivider = 0x00;
	RCC_ClocksTypeDef RCC_ClocksStatus;

	/*---------------------------- USART BRR Configuration -----------------------*/
	/* Configure the USART Baud Rate -------------------------------------------*/
	RCC_GetClocksFreq(&RCC_ClocksStatus);
	if ((uint32_t)s->USARTx == USART1_BASE) {
		apbclock = RCC_ClocksStatus.PCLK2_Frequency;
	}
	else {
		apbclock = RCC_ClocksStatus.PCLK1_Frequency;
	}

	/* Determine the integer part */
	if ((s->USARTx->CR1 & 0x8000) != 0) {
		/* Integer part computing in case Oversampling mode is 8 Samples */
		integerdivider = (25 * apbclock) / (2 * baudrate);
	}
	else /* if ((USARTx->CR1 & CR1_OVER8_Set) == 0) */ {
		/* Integer part computing in case Oversampling mode is 16 Samples */
		integerdivider = (25 * apbclock) / (4 * baudrate);
	}
	tmpreg = (integerdivider / 100) << 4;

	/* Determine the fractional part */
	fractionaldivider = integerdivider - (100 * (tmpreg >> 4));

	/* Implement the fractional part in the register */
	if ((s->USARTx->CR1 & 0x8000) != 0){
		tmpreg |= (((fractionaldivider * 8) + 50) / 100) & ((uint8_t)0x07);
	}
	else /* if ((USARTx->CR1 & CR1_OVER8_Set) == 0) */{
		tmpreg |= (((fractionaldivider * 16) + 50) / 100) & ((uint8_t)0x0F);
	}

	/* Write to USART BRR */
	s->USARTx->BRR = (uint16_t)tmpreg;

}
