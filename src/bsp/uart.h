/*
 * Simple UART port abstraction loosely based on cleanflight serial library
 */

#pragma once

#include <stdbool.h>
#include "stm32f10x_conf.h"

typedef enum uartFormat_t {
    MODE_8N1,
    MODE_8N2,
    MODE_8E1,
    MODE_8E2,
    MODE_8O1,
    MODE_8O2,
} uartFormat_t;

typedef enum uartMode_t {
    MODE_RX = USART_Mode_Rx,
    MODE_TX = USART_Mode_Tx,
    MODE_RXTX = MODE_RX | MODE_TX,
} uartMode_t;

typedef struct {
    uint32_t rxBufferSize;
    uint32_t txBufferSize;
    volatile uint8_t *rxBuffer;
    volatile uint8_t *txBuffer;
    uint32_t txBufferHead;
    uint32_t txBufferTail;

    DMA_Channel_TypeDef *rxDMAChannel;
    DMA_Channel_TypeDef *txDMAChannel;

    uint32_t txDMAIrq;

    uint32_t rxDMAPos;
    bool txDMAEmpty;

    uint32_t txDMAPeripheralBaseAddr;
    uint32_t rxDMAPeripheralBaseAddr;

    USART_TypeDef *USARTx;
} uartPort_t;


// serialPort API
uartPort_t *uart_open(USART_TypeDef *USARTx, uint32_t baudRate, uartFormat_t format, uartMode_t mode);
void uart_write(uartPort_t *s, uint8_t ch);
uint8_t uart_read(uartPort_t *s);
uint8_t uart_bytes_waiting(uartPort_t *s);
void uart_send(uartPort_t *s);
void uart_put(uartPort_t *s, uint8_t ch);
void uart_puts(uartPort_t *s, void *pData, uint32_t len);
void uart_set_baudrate(uartPort_t *s, uint32_t baudrate);
void uart_flush(uartPort_t *s);
