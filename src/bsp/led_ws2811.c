/* (c) 2015, Thomas Raffler */
#include <stdint.h>

#include <stdlib.h>
#include <string.h>

#include "stm32f10x_conf.h"
#include "led_ws2811.h"

#define WS2811_PAUSE 42
#if defined (WS2811_W)
#define WS2811_BITS 32
#else
#define WS2811_BITS 24
#endif
#define WS2811_DMA_BUFFER_SIZE (WS2811_BITS * WS2811_LENGTH + WS2811_PAUSE)

#define WS2811_PWM_0 9
#define WS2811_PWM_1 17

/* WS2812
 * PWM 5: TIM3/1, PB4
 * PWM 6: TIM2/3, PA2
 * TIM1/1N, PA7
 * 1.25 us period, 800 kHz
 * 0: 0.4 us high, 0.85 us low
 * 1: 0.8 us high, 0.45 us low
 *
 *
 * Timer frequency: 8 MHz -> 125 ns timestep
 * Period: 10 -> 1.25 us
 * 0 code: pwm 3 -> 0.375/0.875 us
 * 1 code: pwm 6 -> 0.750/0.500 us
 */

uint8_t ws2811_DMABuffer[WS2811_DMA_BUFFER_SIZE]={0};
volatile uint8_t transfer_flag=0;

void ws2811_init(void) {

	TIM_OCInitTypeDef  TIM_OCInitStructure;
	TIM_TimeBaseInitTypeDef  TimeBase;
	DMA_InitTypeDef DMA_Struct;
	GPIO_InitTypeDef GPIO_Struct;

	// GPIO
	GPIO_Struct.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_Struct.GPIO_Speed = GPIO_Speed_50MHz;
#if defined(WS2811_PWM_CH6)
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
	GPIO_Struct.GPIO_Pin = GPIO_Pin_2;
	GPIO_Init(GPIOA, &GPIO_Struct);
#elif defined(WS2811_TIM1_PA7)
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
	GPIO_Struct.GPIO_Pin = GPIO_Pin_7;
	GPIO_Init(GPIOA, &GPIO_Struct);
	GPIO_PinRemapConfig(GPIO_PartialRemap_TIM1, ENABLE);
#else
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);
	GPIO_Struct.GPIO_Pin = GPIO_Pin_4;
	GPIO_Init(GPIOB, &GPIO_Struct);
	GPIO_PinRemapConfig(GPIO_PartialRemap_TIM3, ENABLE);
#endif

	// timer config
	TimeBase.TIM_ClockDivision = 0;
	TimeBase.TIM_CounterMode = TIM_CounterMode_Up;
	TimeBase.TIM_RepetitionCounter = 0x0000;
	TimeBase.TIM_Prescaler = 3 - 1; // 72 MHz / (TIM_Prescaler + 1) = 24 MHz
	TimeBase.TIM_Period = 30 - 1; // 800 kHz pwm frequency
#if defined(WS2811_PWM_CH6)
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);
	TIM_TimeBaseInit(TIM2, &TimeBase);
#elif defined(WS2811_TIM1_PA7)
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM1, ENABLE);
	TIM_TimeBaseInit(TIM1, &TimeBase);
#else
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE);
	TIM_TimeBaseInit(TIM3, &TimeBase);
#endif

	// output compare
	TIM_OCStructInit(&TIM_OCInitStructure);
	TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1;
	TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
	TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High;
	TIM_OCInitStructure.TIM_Pulse = 0;
#if defined(WS2811_PWM_CH6)
	TIM_OC3Init(TIM2, &TIM_OCInitStructure);
	TIM_OC3PreloadConfig(TIM2, TIM_OCPreload_Enable);
	TIM_Cmd(TIM2, ENABLE);
#elif defined(WS2811_TIM1_PA7)
	TIM_OCInitStructure.TIM_OutputNState = TIM_OutputNState_Enable;
	TIM_OCInitStructure.TIM_OCNPolarity = TIM_OCNPolarity_Low;
	TIM_OCInitStructure.TIM_OCIdleState = TIM_OCIdleState_Reset;
	TIM_OCInitStructure.TIM_OCNIdleState = TIM_OCNIdleState_Reset;
	TIM_OC1NPolarityConfig(TIM1, TIM_OCPolarity_High);

	TIM_OC1Init(TIM1, &TIM_OCInitStructure);
	TIM_OC1PreloadConfig(TIM1, TIM_OCPreload_Enable);
	TIM_CtrlPWMOutputs(TIM1, ENABLE);
	TIM_Cmd(TIM1, ENABLE);
#else
	TIM_OC1Init(TIM3, &TIM_OCInitStructure);
	TIM_OC1PreloadConfig(TIM3, TIM_OCPreload_Enable);
	TIM_Cmd(TIM3, ENABLE);
#endif


	// DMA
	// clock enable */
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1, ENABLE);

#ifdef WS2811_PWM_CH6
	// DMA1 Channel1
	DMA_DeInit(DMA1_Channel1);
#elif defined(WS2811_TIM1_PA7)
	// DMA1 Channel1
	DMA_DeInit(DMA1_Channel2);
#else
	// DMA1 Channel6
	DMA_DeInit(DMA1_Channel6);
#endif

	DMA_StructInit(&DMA_Struct);
#ifdef WS2811_PWM_CH6
	DMA_Struct.DMA_PeripheralBaseAddr = (uint32_t)&TIM2->CCR3;
#elif defined(WS2811_TIM1_PA7)
	DMA_Struct.DMA_PeripheralBaseAddr = (uint32_t)&TIM1->CCR1;
#else
	DMA_Struct.DMA_PeripheralBaseAddr = (uint32_t)&TIM3->CCR1;
#endif
	DMA_Struct.DMA_MemoryBaseAddr = (uint32_t)ws2811_DMABuffer;
	DMA_Struct.DMA_DIR = DMA_DIR_PeripheralDST;
	DMA_Struct.DMA_BufferSize = WS2811_DMA_BUFFER_SIZE;
	DMA_Struct.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
	DMA_Struct.DMA_MemoryInc = DMA_MemoryInc_Enable;
	DMA_Struct.DMA_PeripheralDataSize = DMA_PeripheralDataSize_HalfWord;
	DMA_Struct.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
	DMA_Struct.DMA_Mode = DMA_Mode_Normal;
	DMA_Struct.DMA_Priority = DMA_Priority_High;
	DMA_Struct.DMA_M2M = DMA_M2M_Disable;

#ifdef WS2811_PWM_CH6
	DMA_Init(DMA1_Channel1, &DMA_Struct);

	/* TIM2 CC3 DMA Request enable */
	TIM_DMACmd(TIM2, TIM_DMA_CC3, ENABLE);
	DMA_ITConfig(DMA1_Channel1, DMA_IT_TC, ENABLE);
#elif defined(WS2811_TIM1_PA7)
	DMA_Init(DMA1_Channel2, &DMA_Struct);

	/* TIM1 CC1 DMA Request enable */
	TIM_DMACmd(TIM1, TIM_DMA_CC1, ENABLE);
	DMA_ITConfig(DMA1_Channel2, DMA_IT_TC, ENABLE);
#else
	DMA_Init(DMA1_Channel6, &DMA_Struct);

	/* TIM3 CC1 DMA Request enable */
	TIM_DMACmd(TIM3, TIM_DMA_CC1, ENABLE);
	DMA_ITConfig(DMA1_Channel6, DMA_IT_TC, ENABLE);
#endif


	NVIC_InitTypeDef NVIC_InitStructure;
#ifdef WS2811_PWM_CH6
	NVIC_InitStructure.NVIC_IRQChannel = DMA1_Channel1_IRQn;
#elif defined(WS2811_TIM1_PA7)
	NVIC_InitStructure.NVIC_IRQChannel = DMA1_Channel2_IRQn;
#else
	NVIC_InitStructure.NVIC_IRQChannel = DMA1_Channel6_IRQn;
#endif
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 2;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

	ws2811_clear();
}

void ws2811_transfer(void) {
	if (transfer_flag==0) {
		transfer_flag=1;
#ifdef WS2811_PWM_CH6
		TIM_SetCounter(TIM2, 0);
		DMA_SetCurrDataCounter(DMA1_Channel1, WS2811_DMA_BUFFER_SIZE);
		DMA_Cmd(DMA1_Channel1, ENABLE);
#elif defined(WS2811_TIM1_PA7)
		TIM_SetCounter(TIM1, 0);
		DMA_SetCurrDataCounter(DMA1_Channel2, WS2811_DMA_BUFFER_SIZE);
		DMA_Cmd(DMA1_Channel2, ENABLE);
#else
		TIM_SetCounter(TIM3, 0);
		DMA_SetCurrDataCounter(DMA1_Channel6, WS2811_DMA_BUFFER_SIZE);
		DMA_Cmd(DMA1_Channel6, ENABLE);
#endif
	}
}

void ws2811_clear(void) {
	memset(ws2811_DMABuffer, WS2811_PWM_0, sizeof(ws2811_DMABuffer)-WS2811_PAUSE);
}

void ws2811_pixel(uint8_t *rgb, uint16_t pixel_no) {
	ws2811_set_pixel(rgb, pixel_no);
	ws2811_transfer();
}

void ws2811_set_pixel(uint8_t *rgb, uint16_t pixel_no) {
	uint16_t byte=pixel_no*WS2811_BITS;

	// manually unrolled code is significantly faster...
#if defined(WS2811_RGB)
	// rgb, most significant bit first
	ws2811_DMABuffer[byte++] = (rgb[0] & (1 << 7)) ? WS2811_PWM_1 : WS2811_PWM_0;
	ws2811_DMABuffer[byte++] = (rgb[0] & (1 << 6)) ? WS2811_PWM_1 : WS2811_PWM_0;
	ws2811_DMABuffer[byte++] = (rgb[0] & (1 << 5)) ? WS2811_PWM_1 : WS2811_PWM_0;
	ws2811_DMABuffer[byte++] = (rgb[0] & (1 << 4)) ? WS2811_PWM_1 : WS2811_PWM_0;
	ws2811_DMABuffer[byte++] = (rgb[0] & (1 << 3)) ? WS2811_PWM_1 : WS2811_PWM_0;
	ws2811_DMABuffer[byte++] = (rgb[0] & (1 << 2)) ? WS2811_PWM_1 : WS2811_PWM_0;
	ws2811_DMABuffer[byte++] = (rgb[0] & (1 << 1)) ? WS2811_PWM_1 : WS2811_PWM_0;
	ws2811_DMABuffer[byte++] = (rgb[0] & (1 << 0)) ? WS2811_PWM_1 : WS2811_PWM_0;

	ws2811_DMABuffer[byte++] = (rgb[1] & (1 << 7)) ? WS2811_PWM_1 : WS2811_PWM_0;
	ws2811_DMABuffer[byte++] = (rgb[1] & (1 << 6)) ? WS2811_PWM_1 : WS2811_PWM_0;
	ws2811_DMABuffer[byte++] = (rgb[1] & (1 << 5)) ? WS2811_PWM_1 : WS2811_PWM_0;
	ws2811_DMABuffer[byte++] = (rgb[1] & (1 << 4)) ? WS2811_PWM_1 : WS2811_PWM_0;
	ws2811_DMABuffer[byte++] = (rgb[1] & (1 << 3)) ? WS2811_PWM_1 : WS2811_PWM_0;
	ws2811_DMABuffer[byte++] = (rgb[1] & (1 << 2)) ? WS2811_PWM_1 : WS2811_PWM_0;
	ws2811_DMABuffer[byte++] = (rgb[1] & (1 << 1)) ? WS2811_PWM_1 : WS2811_PWM_0;
	ws2811_DMABuffer[byte++] = (rgb[1] & (1 << 0)) ? WS2811_PWM_1 : WS2811_PWM_0;
#elif defined(WS2811_GRB)
	// grb, most significant bit first
	ws2811_DMABuffer[byte++] = (rgb[1] & (1 << 7)) ? WS2811_PWM_1 : WS2811_PWM_0;
	ws2811_DMABuffer[byte++] = (rgb[1] & (1 << 6)) ? WS2811_PWM_1 : WS2811_PWM_0;
	ws2811_DMABuffer[byte++] = (rgb[1] & (1 << 5)) ? WS2811_PWM_1 : WS2811_PWM_0;
	ws2811_DMABuffer[byte++] = (rgb[1] & (1 << 4)) ? WS2811_PWM_1 : WS2811_PWM_0;
	ws2811_DMABuffer[byte++] = (rgb[1] & (1 << 3)) ? WS2811_PWM_1 : WS2811_PWM_0;
	ws2811_DMABuffer[byte++] = (rgb[1] & (1 << 2)) ? WS2811_PWM_1 : WS2811_PWM_0;
	ws2811_DMABuffer[byte++] = (rgb[1] & (1 << 1)) ? WS2811_PWM_1 : WS2811_PWM_0;
	ws2811_DMABuffer[byte++] = (rgb[1] & (1 << 0)) ? WS2811_PWM_1 : WS2811_PWM_0;

	ws2811_DMABuffer[byte++] = (rgb[0] & (1 << 7)) ? WS2811_PWM_1 : WS2811_PWM_0;
	ws2811_DMABuffer[byte++] = (rgb[0] & (1 << 6)) ? WS2811_PWM_1 : WS2811_PWM_0;
	ws2811_DMABuffer[byte++] = (rgb[0] & (1 << 5)) ? WS2811_PWM_1 : WS2811_PWM_0;
	ws2811_DMABuffer[byte++] = (rgb[0] & (1 << 4)) ? WS2811_PWM_1 : WS2811_PWM_0;
	ws2811_DMABuffer[byte++] = (rgb[0] & (1 << 3)) ? WS2811_PWM_1 : WS2811_PWM_0;
	ws2811_DMABuffer[byte++] = (rgb[0] & (1 << 2)) ? WS2811_PWM_1 : WS2811_PWM_0;
	ws2811_DMABuffer[byte++] = (rgb[0] & (1 << 1)) ? WS2811_PWM_1 : WS2811_PWM_0;
	ws2811_DMABuffer[byte++] = (rgb[0] & (1 << 0)) ? WS2811_PWM_1 : WS2811_PWM_0;
#endif
	ws2811_DMABuffer[byte++] = (rgb[2] & (1 << 7)) ? WS2811_PWM_1 : WS2811_PWM_0;
	ws2811_DMABuffer[byte++] = (rgb[2] & (1 << 6)) ? WS2811_PWM_1 : WS2811_PWM_0;
	ws2811_DMABuffer[byte++] = (rgb[2] & (1 << 5)) ? WS2811_PWM_1 : WS2811_PWM_0;
	ws2811_DMABuffer[byte++] = (rgb[2] & (1 << 4)) ? WS2811_PWM_1 : WS2811_PWM_0;
	ws2811_DMABuffer[byte++] = (rgb[2] & (1 << 3)) ? WS2811_PWM_1 : WS2811_PWM_0;
	ws2811_DMABuffer[byte++] = (rgb[2] & (1 << 2)) ? WS2811_PWM_1 : WS2811_PWM_0;
	ws2811_DMABuffer[byte++] = (rgb[2] & (1 << 1)) ? WS2811_PWM_1 : WS2811_PWM_0;
	ws2811_DMABuffer[byte++] = (rgb[2] & (1 << 0)) ? WS2811_PWM_1 : WS2811_PWM_0;
#if defined(WS2811_W)
	// white, most significant bit first
	ws2811_DMABuffer[byte++] = (rgb[3] & (1 << 7)) ? WS2811_PWM_1 : WS2811_PWM_0;
	ws2811_DMABuffer[byte++] = (rgb[3] & (1 << 6)) ? WS2811_PWM_1 : WS2811_PWM_0;
	ws2811_DMABuffer[byte++] = (rgb[3] & (1 << 5)) ? WS2811_PWM_1 : WS2811_PWM_0;
	ws2811_DMABuffer[byte++] = (rgb[3] & (1 << 4)) ? WS2811_PWM_1 : WS2811_PWM_0;
	ws2811_DMABuffer[byte++] = (rgb[3] & (1 << 3)) ? WS2811_PWM_1 : WS2811_PWM_0;
	ws2811_DMABuffer[byte++] = (rgb[3] & (1 << 2)) ? WS2811_PWM_1 : WS2811_PWM_0;
	ws2811_DMABuffer[byte++] = (rgb[3] & (1 << 1)) ? WS2811_PWM_1 : WS2811_PWM_0;
	ws2811_DMABuffer[byte++] = (rgb[3] & (1 << 0)) ? WS2811_PWM_1 : WS2811_PWM_0;
#endif
}

#ifdef WS2811_PWM_CH6
void DMA1_Channel1_IRQHandler(void)
{
	// stop dma pwm
	if (DMA_GetFlagStatus(DMA1_FLAG_TC1)) {
		DMA_Cmd(DMA1_Channel1, DISABLE);
		DMA_ClearFlag(DMA1_FLAG_TC1);
		transfer_flag=0;
	}
}
#elif defined(WS2811_TIM1_PA7)
void DMA1_Channel2_IRQHandler(void)
{
	// stop dma pwm
	if (DMA_GetFlagStatus(DMA1_FLAG_TC2)) {
		DMA_Cmd(DMA1_Channel2, DISABLE);
		DMA_ClearFlag(DMA1_FLAG_TC2);
		transfer_flag=0;
	}
}
#else
void DMA1_Channel6_IRQHandler(void)
{
	// stop dma pwm
	if (DMA_GetFlagStatus(DMA1_FLAG_TC6)) {
		DMA_Cmd(DMA1_Channel6, DISABLE);
		DMA_ClearFlag(DMA1_FLAG_TC6);
		transfer_flag=0;
	}
}
#endif
