#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Update a simple plot with incoming data
"""
import sys
from pyqtgraph.Qt import QtGui, QtCore
from PyQt5.QtWidgets import QMessageBox
import numpy as np
import pyqtgraph as pg
import mcucom
import struct
import time
import os.path

portstr = 'VID:PID=0483:5740'
baudrate = 250000
sampletime_ms = 50
sample_nr = 5000
fRaw = None
t = 0
cmd_min = 138 # 1000 us, 10 us/LSB
cmd_off = 128 # 900 us
mcu = mcucom.mcucom(portstr, baudrate)

if not os.path.isdir("logs"):
    os.mkdir("logs")
    print("Created folder 'logs'.")

class ScopeData:
    """Scope data class"""
    def __init__(self, no_of_samples):
        self.data = no_of_samples*[np.nan]
        self.pos = 0
        self.no_of_samples = no_of_samples
    def push(self, new_samples):
        # add new samples
        for sample in new_samples:
            self.data[self.pos] = sample
            self.pos += 1
            if self.pos >= self.no_of_samples:
                self.pos = 0
        # delete one sample to disconnect line
        self.data[self.pos] = np.nan
    
def btn_off_clicked():
    mcu.write([0])
    spinbox.setValue(0)
    
def btn_cmd_clicked():
    mcu.write([combo.currentIndex()])
    
def btn_step1_pressed():
    mcu.write(b'j')
    target = cmd_min+spinbox.value()+10
    if target > cmd_min+10 and target < cmd_min+110:
        mcu.write([target])
    
def btn_step1_released():
    mcu.write([cmd_min+spinbox.value()])
    
def btn_step2_pressed():
    mcu.write(b'j')
    target = cmd_min+spinbox.value()+20
    if target > cmd_min+20 and target < cmd_min+110:
        mcu.write([target])
    
def btn_step2_released():
    mcu.write([cmd_min+spinbox.value()])
    
def btn_up_pressed():
    mcu.write(b'r')
    mcu.write([cmd_min+spinbox2.value()])
    
def btn_up_released():
    mcu.write([cmd_min+spinbox.value()])
    
def spinbox_changed():
    if spinbox.value() > 0:
        mcu.write([cmd_min+spinbox.value()])
    else:
        mcu.write([cmd_off])

def checkbox_changed():
    global fRaw
    if tick_logging.isChecked():
        try:
            logName = time.strftime("%Y-%m-%d_%H-%M", time.localtime(time.time()))
            fRaw = open(os.path.join('logs',logName+'.uart'), 'wb')
            print("Logfile opened: "+logName+'.uart')
        except Exception as detail:
            fRaw = None
            print("Logfile could not be opened: "+str(detail))
    elif fRaw != None:
        fRaw.close()
        print("Logfile closed. ")
        
## Start Qt event loop unless running in interactive mode.
if __name__ == '__main__':
    app = QtGui.QApplication(sys.argv)
    win = QtGui.QWidget()
    ## Create some widgets to be placed inside
    tick_logging = QtGui.QCheckBox('Logging')
    btn_off = QtGui.QPushButton('off')
    btn_step1 = QtGui.QPushButton('Step 10%')
    btn_step2 = QtGui.QPushButton('Step 20%')
    btn_up = QtGui.QPushButton('Ramp up')
    btn_cmd = QtGui.QPushButton('cmd')
    btn_sequence = QtGui.QPushButton('Test sequence')
    edit_sequence = QtGui.QLineEdit('50')
    edit_sequence.setValidator(QtGui.QDoubleValidator(1,100,0))
    combo = QtGui.QComboBox()
    combo.addItems(["0  MOTOR_STOP", "1  BEEP1", "2  BEEP2", "3  BEEP3", "4  BEEP4", "5  BEEP5", 
                    "6  ESC_INFO", "7  SPIN_DIRECTION_1 (6x)", "8  SPIN_DIRECTION_2 (6x)", 
                    "9  3D_MODE_OFF (6x)", "10 3D_MODE_ON (6x)", "11 SETTINGS_REQUEST", 
                    "12 SAVE_SETTINGS (6x)", "20 SPIN_DIRECTION_NORMAL (6x)", 
                    "21 SPIN_DIRECTION_REVERSED (6x)"] )
    text = QtGui.QLineEdit('')
    text.setFont(QtGui.QFont('Monospace'))
    spinbox = QtGui.QSpinBox()
    spinbox2 = QtGui.QSpinBox()
    labelRPM = QtGui.QLabel('RPM Divisor')
    editDivRPM = QtGui.QLineEdit('0.07')
    editDivRPM.setValidator(QtGui.QDoubleValidator(0.01,10,2))
    labelU = QtGui.QLabel('Scalefactor U')
    editScaleU = QtGui.QLineEdit('0.01')
    editScaleU.setValidator(QtGui.QDoubleValidator(0.0001,0.2,5))
    labelI = QtGui.QLabel('Scalefactor I')
    editScaleI = QtGui.QLineEdit('0.01')
    editScaleI.setValidator(QtGui.QDoubleValidator(0.0001,0.2,5))
    labelC = QtGui.QLabel('Scalefactor C')
    editScaleC = QtGui.QLineEdit('1')
    editScaleC.setValidator(QtGui.QDoubleValidator(0.1,10,2))
    plot = pg.GraphicsLayoutWidget()
    
    
    btn_off.clicked.connect(btn_off_clicked)
    btn_cmd.clicked.connect(btn_cmd_clicked)
    btn_step1.pressed.connect(btn_step1_pressed)
    btn_step1.released.connect(btn_step1_released)
    btn_step2.pressed.connect(btn_step2_pressed)
    btn_step2.released.connect(btn_step2_released)
    btn_up.pressed.connect(btn_up_pressed)
    btn_up.released.connect(btn_up_released)
    spinbox.valueChanged.connect(spinbox_changed)
    tick_logging.stateChanged.connect(checkbox_changed)
    
    spinbox.setRange(0, 100)
    spinbox.setSuffix('%')
    spinbox.setSingleStep(5)
    spinbox2.setRange(10, 100)
    spinbox2.setSuffix('%')
    spinbox2.setSingleStep(10)
    ## Create a grid layout to manage the widgets size and position
    layout = QtGui.QGridLayout()
    win.setLayout(layout)
    
    ## Add widgets to the layout in their proper positions
    layout.addWidget(tick_logging, 0, 0)
    layout.addWidget(spinbox, 0, 1)
    layout.addWidget(btn_step1, 0, 2)
    layout.addWidget(btn_sequence, 0, 3)
    #layout.setColumnStretch(3, 1)
    layout.setColumnStretch(4, 1)
    layout.addWidget(combo, 0, 5)
    layout.addWidget(labelRPM, 0, 6)
    layout.addWidget(editDivRPM, 0, 7)
    layout.addWidget(labelU, 0, 8)
    layout.addWidget(editScaleU, 0, 9)
    layout.addWidget(btn_off, 1, 1)
    #layout.addWidget(spinbox2, 1, 2)
    layout.addWidget(btn_step2, 1, 2)
    layout.addWidget(edit_sequence, 1, 3)
    #layout.addWidget(btn_up, 1, 1)
    layout.addWidget(btn_cmd, 1, 5)
    # setting
    layout.addWidget(labelI, 1, 6)
    layout.addWidget(editScaleI, 1, 7)
    layout.addWidget(labelC, 1, 8)
    layout.addWidget(editScaleC, 1, 9)
    # text spans 10 cols
    layout.addWidget(text, 2, 0, 1, 10)
    # plot spans 10 cols
    layout.addWidget(plot, 3, 0, 1, 10)  
    
    win.show()
    win.resize(1200,600)

    plot1 = plot.addPlot()
    plot1.setRange(QtCore.QRectF(0, 0, sample_nr, 60)) 
    plot1.setLabel('bottom', 'Index', units='samples')
    plot1.setTitle('Voltage, Current')
    curve1 = plot1.plot(pen='y')
    curve2 = plot1.plot(pen='c')
    
    plot2 = plot.addPlot()
    plot2.setRange(QtCore.QRectF(0, 750, sample_nr, 12000)) 
    plot2.setLabel('bottom', 'Index', units='samples')
    plot2.setTitle('PWM, RPM')
    curve3 = plot2.plot(pen='g')
    curve4 = plot2.plot(pen='r')
    
    scope1 = ScopeData(sample_nr)
    scope2 = ScopeData(sample_nr)
    scope3 = ScopeData(sample_nr)
    scope4 = ScopeData(sample_nr)
    
    
    sequencer = 0
    sequence_index = 0

    def update():
        global t, sequence_index, sequencer

        test_sequence = [10,20,30,40,50,60,70,80,90,10,30,60,90,30,50,70,90,20,50,80,20,40,\
            60,80,10,40,70,10,50,90,40,80,30,70,20,60,10]
        test_sequence_r = list(reversed(test_sequence))
        test_sequence = test_sequence + test_sequence_r
        timediv = 10
        if btn_sequence.isDown():
            if sequencer > timediv-2:
                sequencer = 0
                if sequence_index < len(test_sequence)-1:
                    sequence_index += 1
                    mcu.write(b'j')
                    mcu.write([cmd_min+test_sequence[sequence_index]])
            else:
                sequencer += 1
        else:
            sequencer = 0
            if sequence_index > 0:
                mcu.write(b'j')
                mcu.write([cmd_min])
                sequence_index = 0
        
        try:
            # heartbeat 
            mcu.write(b'h')
            # read and log data
            rawData = mcu.read()
            if fRaw != None and fRaw.closed != True:
                fRaw.write(rawData)
        except Exception as detail:
            pass

        logstr = ''
        for frame in mcu.parse():
            if frame[0] == ord('m') and len(frame[1])==16:
                drive_data = list(struct.unpack("IHBbHhHH", frame[1]))
                pwm = drive_data[1]
                T = drive_data[3]
                U = drive_data[4]*float(editScaleU.text())
                I = drive_data[5]*float(editScaleI.text())
                C = drive_data[6]*float(editScaleC.text())
                rpm = drive_data[7]/float(editDivRPM.text())
                scope1.push([pwm])
                scope2.push([U])
                scope3.push([I])
                scope4.push([rpm])
                logstr = f"{U:2.2f} V, {I:2.2f} A, {C:4.0f} mAh, {rpm:5.0f} RPM, {T:2d} °C, {pwm:4d} us"
        curve1.setData(scope2.data, connect="finite")
        curve2.setData(scope3.data, connect="finite")
        curve3.setData(scope1.data, connect="finite")
        curve4.setData(scope4.data, connect="finite")
        if logstr != '':
            text.setText(logstr)
        t += sampletime_ms/1000.0
    
    if mcu.uart == None:
        msg = QtGui.QMessageBox(QMessageBox.Information, "Connection", 
            "Test device could not be connected", QtGui.QMessageBox.Ok)
        msg.exec_()
    
    timer = QtCore.QTimer()
    timer.timeout.connect(update)
    timer.start(sampletime_ms)
    
    if (sys.flags.interactive != 1) or not hasattr(QtCore, 'PYQT_VERSION'):
        QtGui.QApplication.instance().exec_()
