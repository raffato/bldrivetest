#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Communicate with mcu via uart

(c) 2018 Thomas Raffler
"""

import serial
import serial.tools.list_ports
import crc16
import struct

class mcucom:
    def __init__(self, portstr='auto', baudrate=1000000, timeout=0.1):
        self.buffer = b''
        self.parsed_frames = 0
        self.bad_frames = 0
        self.read_bytes = 0
        self.dropped_bytes = 0
        # open serial stream
        ports = serial.tools.list_ports.comports()
        for port, desc, hwid in sorted(ports):
            print("{}: {} [{}]".format(port, desc, hwid))
            if portstr == 'auto' or portstr in hwid:
                portstr = port
        try:
            self.uart = serial.Serial(portstr, baudrate, timeout=timeout)
        except Exception as detail:
            print("Port could not be opened: " + str(detail))
            self.uart = None
    def send(self, msg_id, payload):
        frame_data = b'\nn' + msg_id + bytes([len(payload)]) + payload
        try:
            self.uart.write(frame_data + struct.pack("<H", crc16.xmodem(frame_data[2:len(frame_data)])))
            return True
        except:
            return False
        
    def get_statistics(self):
        return ('Found {0} valid frames and {1} crc errors in {2} bytes. Dropped {3} bytes.'\
                .format(self.parsed_frames, self.bad_frames, self.read_bytes, self.dropped_bytes))

    def read(self, chunk_size=16384):
        if self.uart is not None:
            chunk_size = min([self.uart.in_waiting, int(chunk_size)])
            chunk = self.uart.read(chunk_size)
            self.buffer = self.buffer + chunk
            self.read_bytes += len(chunk)
        else:
            chunk=b''
        return chunk
        
    def write(self, data):
        if self.uart is not None:
            return self.uart.write(data)
            
    def parse(self, STARTBYTE1=ord('\n'), STARTBYTE2=ord('n')):
        frame_list = []
        while len(self.buffer) > 5:
            # frame synchronization
            if self.buffer[0] == STARTBYTE1 and self.buffer[1] == STARTBYTE2 and len(self.buffer) >= self.buffer[3]+6:
                # try to read frame
                frame_id = self.buffer[2]
                payload_length = self.buffer[3]
                # check CRC
                CRC16 = crc16.xmodem(self.buffer[2:payload_length+4])
                crc_rx = self.buffer[payload_length+4] + self.buffer[payload_length+5]*256
                if (CRC16 == crc_rx):
                    frame_data = self.buffer[4:payload_length+4]
                    frame_list.append((frame_id, frame_data))
                    # remove parsed frame
                    self.parsed_frames += 1
                    self.buffer = self.buffer[payload_length+6:len(self.buffer)]
                else:
                    # move forward
                    self.buffer = self.buffer[1:len(self.buffer)]
                    self.bad_frames += 1
                    self.dropped_bytes += 1
                    
                    if self.bad_frames < 21:
                        print('Sent CRC: {0:04x}, Computed: {1:04x}'\
                            .format(crc_rx, CRC16))
                    elif self.bad_frames == 21:
                        print('Further CRC errors not displayed...')
                    
            elif self.buffer[0] != STARTBYTE1 or self.buffer[1] != STARTBYTE2:
                # startbytes not found, drop first byte
                self.buffer = self.buffer[1:len(self.buffer)]
                self.dropped_bytes += 1
            else:
                # need more data
                break
        return frame_list
